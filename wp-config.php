<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'home_wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Abc123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FTP_USER', 'ftp_user');
define('FTP_PASS', 'passwordforftp_user');
define('FTP_HOST', 'faacclub.com');
define('FTP_SSL', false);
define('FS_METHOD','direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/$^*{|BbHErAC|1B[qZJf#`JN=fV%e=A&`;`@~H@zyz(F8B03/Bs~%y{.aQS&+.q' );
define( 'SECURE_AUTH_KEY',  'iYQSpgKpB-O`)X~DFaE1ubvpKT3a!;PxUbZ>V}=%]=7L)8vLaul:)=!;lV/e^%^T' );
define( 'LOGGED_IN_KEY',    'lv^>vnn%yCE?D_]t|&iG7UYZ@0x#%)~t?meUH1BD:e ?&Xv@JOM}} uL,A~vf9T-' );
define( 'NONCE_KEY',        '6z2:}11@l*=eVD]H+@v%Q0c:09-=^xPRJzCV ws q$#a7Xlda-+;Yo-[@WBIWgPp' );
define( 'AUTH_SALT',        '+VW}VBDgh~a;8ajU*W2_#w,m,x/cf@I^kDR}erSUX4GXK0X+!,,9ylj)JLu6Vn{K' );
define( 'SECURE_AUTH_SALT', 'w.8S{XU(~{WHMIlJDLy^0.@i!0b#g))~[dzQWHb7LVbkFP,(N8TQ@7ky@yk>=tyj' );
define( 'LOGGED_IN_SALT',   '7#]dO$tEt5>Fn,Wod5vvynVF.:Eijk?=^V4fIpZgB2Fv!_GAK#C<8SKl,E{BHRVe' );
define( 'NONCE_SALT',       'mBo#(3bo5p9Vr-<ZhZ1Qf/{XVmY$a[,j56MM/oqC3|NEKtx->%*G!,qs-K,F{B^m' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'home_wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
